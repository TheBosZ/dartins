@JS()
library app_settings;

import 'package:node_interop/util.dart';
import 'package:js/js.dart';
import 'dart:async';
import 'dart:js';

@JS()
class AppSettings {
	external static get(Function success, Function failure, List<String> preferences);
}

class AppSettingsHelper {
	static Future<Map> get(List<String> preferences) {
		var c = Completer();
		AppSettings.get(allowInterop((var result){
			var obj = dartify(result);
			if (obj != null && obj.containsKey('o')) {
				obj = obj['o'];
			}
			c.complete(obj);
		}), allowInterop((var result){
			c.completeError(result);
		}), preferences);
		return c.future;
	}
}
