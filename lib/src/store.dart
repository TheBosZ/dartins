@JS()
library store;

import 'package:js/js.dart';

@JS('CdvPurchase.store')
class Store {

	static const int QUIET = 0;
	static const int ERROR = 1;
	static const int WARNING = 2;
	static const int INFO = 3;
	static const int DEBUG = 4;

	external static int get verbosity;

	external static set verbosity(int v);

	external static String get validator;

	external static set validator(String v);

	external static List<StoreProduct> get products;

	external static register(StoreProduct product);

	external static ProductEvent when([String v]);

	external static Future initialize();

	external static Future update();

	external static restorePurchases();

	external static order(String v);

	external static ready(Function v);

	external static owned(String productName);

	external static StoreProduct get(String productName);
}

@JS()
class StorePlatform {
	static const String APPLE_APPSTORE = 'ios-appstore'; 
	static const String GOOGLE_PLAY = 'android-playstore'; 
	static const String WINDOWS_STORE = 'windows-store-transaction'; 
	static const String BRAINTREE = 'braintree'; 
	static const String TEST = 'test';
}

@JS()
@anonymous
class StoreProduct {

	static const String APPLICATION = 'application';
	static const String FREE_SUBSCRIPTION = 'free subscription';
	static const String PAID_SUBSCRIPTION = 'paid subscription';
	static const String NON_RENEWING_SUBSCRIPTION = 'non renewing subscription';
	static const String CONSUMABLE = 'consumable';
	static const String NON_CONSUMABLE = 'non consumable';

	external String get id;

	external set id(String v);

	external String get alias;

	external set alias(String v);

	external String get type;

	external set type(String v);

	external String get platform;

	external set platform(String v);

	external bool get canPurchase;

	external String get currency;

	external String get description;

	external bool get downloaded;

	external bool get downloading;

	external bool get loaded;

	external bool get owned;

	external String get price;

	external String get state;

	external String get title;

	external String get transaction;

	external bool get valid;

	external finish();

	external StoreOffer getOffer();

	external factory StoreProduct({
		String id,
		String type,
		String alias,
		String platform,
	});
}

@JS()
class ProductEvent {

	external loaded(Function v);

	external updated(Function v);

	external approved(Function v);

	external error(Function v);

	external owned(Function v);

	external cancelled(Function v);

	external refunded(Function v);

	external verified(Function v);

	external productUpdated(Function v);

	external receiptUpdated(Function v);
}

@JS()
class StoreOffer {
	external order();
}
