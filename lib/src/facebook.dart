@JS()
library facebook;

import 'package:js/js.dart';
import 'dart:async';
import 'package:node_interop/util.dart';

class Facebook {


	static Future<Map> login(List<String> perms) {
		var c = Completer();
		FacebookApi.login(perms, allowInterop((var result){
			var obj = dartify(result);
			c.complete(Facebook._processResult(obj));
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	static Future logout(){
		var c = Completer();
		FacebookApi.logout(allowInterop((var result){
			c.complete(result);
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	static Future<Map> getLoginStatus() {
		var c = Completer();
		FacebookApi.getLoginStatus(allowInterop((var result){
			var obj = dartify(result);

			c.complete(Facebook._processResult(obj));
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	static Future showDialog(DialogOptions options){
		var c = Completer();
		FacebookApi.showDialog(options, allowInterop((var result){
			c.complete(result);
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	static Future<Map> api(String requestPath, List<String> perms) {
		var c = Completer();
		FacebookApi.api(requestPath, perms, allowInterop((var result){
			var obj = dartify(result);

			c.complete(Facebook._processResult(obj));
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	static Future logEvent(String name, Object params, num valueToSum) {
		var c = Completer();
		FacebookApi.logEvent(name, params, valueToSum, allowInterop((var result){
			c.complete(result);
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	static Future logPurchase(num value, String currency) {
		var c = Completer();
		FacebookApi.logPurchase(value, currency, allowInterop((var result){
			c.complete(result);
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	static Future activateApp() {
		var c = Completer();
		FacebookApi.activateApp(allowInterop((var result){
			c.complete(result);
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	static Future appInvite(Object options) {
		var c = Completer();
		FacebookApi.appInvite(options, allowInterop((var result){
			c.complete(result);
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	static Future browserInit(String appId, [String version]) {
		var c = Completer();
		FacebookApi.browserInit(appId, version, allowInterop((var result){
			c.complete(result);
		}));
		return c.future;
	}

	static Map _processResult(Map obj) {
		if (obj == null) {
			return obj;
		}
		if (obj.containsKey('o')) {
			return obj['o'];
		}
		return obj;
	}
}

@JS('facebookConnectPlugin')
class FacebookApi {
	external static login(List<String> perms, Function success, Function failure);
	external static logout(Function success, Function failure);
	external static getLoginStatus(Function success, Function failure);
	external static showDialog(DialogOptions options, Function success, Function failure);
	external static api(String requestPath, List<String> perms, Function success, Function failure);
	external static logEvent(String name, Object params, num valueToSum, Function success, Function failure);
	external static logPurchase(num value, String currency, Function success, Function failure);
	external static activateApp(Function success, Function failure);
	external static appInvite(Object options, Function success, Function failure);
	external static browserInit(String appId, [String version, Function success]);
}

@JS()
@anonymous
class DialogOptions {
	external String get method;
	external set method(String v);

	external String get href;
	external set href(String v);

	external String get caption;
	external set caption(String v);

	external String get description;
	external set description(String v);

	external String get picture;
	external set picture(String v);

	external bool get share_feedWeb;
	external set share_feedWeb(bool v);

	external factory DialogOptions({
		String method,
		String href,
		String caption,
		String description,
		String picture,
		String quote,
		bool share_feedWeb = false,
		bool share_sheet = false,
		bool share_feedBrowser = false,
		bool share_native = false
	});
}

