@JS()
library ironsource;

import 'package:js/js.dart';
import 'dart:async';

class IronSource {

	static const String onInterstitialAdClicked = 'onInterstitialAdClicked';
	static const String onInterstitialAdClosed = 'onInterstitialAdClosed';
	static const String onInterstitialAdLoadFailed = 'onInterstitialAdLoadFailed';
	static const String onInterstitialAdOpened = 'onInterstitialAdOpened';
	static const String onInterstitialAdReady = 'onInterstitialAdReady';
	static const String onInterstitialAdShowFailed = 'onInterstitialAdShowFailed';
	static const String onInterstitialAdShowSucceeded = 'onInterstitialAdShowSucceeded';
	static const String onOfferwallAdCredited = 'onOfferwallAdCredited';
	static const String onGetOfferwallCreditsFailed = 'onGetOfferwallCreditsFailed';
	static const String onOfferwallAvailable = 'onOfferwallAvailable';
	static const String onOfferwallClosed = 'onOfferwallClosed';
	static const String onOfferwallOpened = 'onOfferwallOpened';
	static const String onOfferwallShowFailed = 'onOfferwallShowFailed';
	static const String onRewardedVideoAdClosed = 'onRewardedVideoAdClosed';
	static const String onRewardedVideoAdEnded = 'onRewardedVideoAdEnded';
	static const String onRewardedVideoAdOpened = 'onRewardedVideoAdOpened';
	static const String onRewardedVideoAdRewarded = 'onRewardedVideoAdRewarded';
	static const String onRewardedVideoAdShowFailed = 'onRewardedVideoAdShowFailed';
	static const String onRewardedVideoAdStarted = 'onRewardedVideoAdStarted';
	static const String onRewardedVideoAvailabilityChanged = 'onRewardedVideoAvailabilityChanged';
	static const String onRewardedVideoAdClicked = 'onRewardedVideoAdClicked'; 

	IronSourceAds _ironSourceAds;
	bool _isInited = false;
	bool _isIniting = false;
	final String _appKey;
	String _userId;
	bool _isTestMode;

	IronSource(this._appKey, [this._userId,this._isTestMode]) {
		_userId ??= 'user_${DateTime.now()}';
		_isTestMode ??= false;
	}

	Future init() {
		if (_isIniting) {
			return Future.error('Already initing');
		}
		if (!_isInited) {
			var c = Completer();
			_isIniting = true;
			_ironSourceAds = IronSourceAds(_appKey, _userId, allowInterop((var result){
				_isIniting = false;
				_isInited = true;
				c.complete();
			}),
			_isTestMode);
			return c.future;
		}
		return Future.value();
	}

	Future<void> showRewardedVideo([String placementName]) {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}

		var c = Completer();
		_ironSourceAds.showRewardedVideo(placementName, allowInterop((){
			c.complete();
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	Future getRewardedVideoPlacementInfo([String placementName]) {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}

		var c = Completer();
		_ironSourceAds.getRewardedVideoPlacementInfo(placementName, allowInterop((var result){
			c.complete(result);
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	Future<bool> isRewardedVideoPlacementCapped([String placementName]) {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}

		var c = Completer();
		_ironSourceAds.isRewardedVideoPlacementCapped(placementName, allowInterop((){
			c.complete(true);
		}), allowInterop((){
			c.complete(false);
		}));
		return c.future;
	}

	Future<bool> isRewardedVideoAvailable() {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}

		var c = Completer();
		_ironSourceAds.isRewardedVideoAvailable(allowInterop((){
			c.complete(true);
		}), allowInterop((){
			c.complete(false);
		}));
		return c.future;
	}

	Future<void> validateIntegration() {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}

		var c = Completer();
		_ironSourceAds.validateIntegration(allowInterop((_){
			c.complete();
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	Future<void> setDynamicUserId(String userId) {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}

		var c = Completer();
		_ironSourceAds.setDynamicUserId(userId, allowInterop((){
			c.complete();
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	Future<void> loadInterstitial() {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}
		
		var c = Completer();
		_ironSourceAds.loadInterstitial(allowInterop((_){
			c.complete();
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	Future<bool> isInterstitialReady() {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}
		
		var c = Completer();
		_ironSourceAds.isInterstitialReady(allowInterop((_){
			c.complete(true);
		}), allowInterop((_){
			c.complete(false);
		}));
		return c.future;
	}

	Future getInterstitialPlacementInfo([String placementName]) {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}
		
		var c = Completer();
		_ironSourceAds.isInterstitialReady(allowInterop((var result){
			c.complete(result);
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	Future<void> showInterstitial([String placementName]) {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}
		
		var c = Completer();
		_ironSourceAds.showInterstitial(placementName, allowInterop((){
			c.complete();
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	Future<void> showOfferwall([String placementName]) {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}
		
		var c = Completer();
		_ironSourceAds.showOfferwall(placementName, allowInterop((){
			c.complete();
		}), allowInterop((var result){
			c.completeError(result);
		}));
		return c.future;
	}

	Future<bool> isOfferwallAvailable() {
		if (!_isInited) {
			return Future.error('Please call init() first');
		}
		
		var c = Completer();
		_ironSourceAds.isOfferwallAvailable(allowInterop((){
			c.complete(true);
		}), allowInterop((){
			c.complete(false);
		}));
		return c.future;
	}
}

@JS('IronSourceAds')
class IronSourceAds {
	external IronSourceAds(String appKey, String userId, [Function success,bool isTestMode]);
	external showRewardedVideo(
		[String placementName, Function success, Function error]);
	external getRewardedVideoPlacementInfo(
		[String placementName, Function success, Function error]);
	external isRewardedVideoPlacementCapped(
		[String placementName, Function success, Function error]);
	external isRewardedVideoAvailable([Function success, Function error]);
	external validateIntegration([Function success, Function error]);
	external setDynamicUserId(String userId, [Function success, Function error]);
	external loadInterstitial([Function success, Function error]);
	external isInterstitialReady([Function success, Function error]);
	external getInterstitialPlacementInfo(
		[String placementName, Function success, Function error]);
	external showInterstitial([String placementName, Function success, Function error]);
	external showOfferwall([String placementName, Function success, Function error]);
	external isOfferwallAvailable([Function success, Function error]);
}
